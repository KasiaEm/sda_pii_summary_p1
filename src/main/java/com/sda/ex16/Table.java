package com.sda.ex16;

final class Table extends Furniture {
    private final double length;
    private final double width;
    private final double height;

    public Table(double length, double width, double height, Double price) {
        super(price);
        this.length = length;
        this.width = width;
        this.height = height;
    }

    @Override
    public Double area() {
        return length * width;
    }

    @Override
    public String show() {
        return "Table, w: " + width + " l: " + length + " h: " + height;
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }
}
