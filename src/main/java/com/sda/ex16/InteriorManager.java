package com.sda.ex16;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class InteriorManager {
    private List<Measurable> resources = new ArrayList<>();
    private Logger logger = Logger.getLogger(InteriorManager.class.getName());

    // returns all recources
    public List<Measurable> getResources() {
        return resources;
    }

    // adds room with furniture if possible
    public void addRoomWithFurniture(RectangularRoom room, Furniture... furnitures) {
        Double furnitureSummaryArea = 0d;
        Double summaryPrice = 0d;
        for (Furniture f : furnitures) {
            furnitureSummaryArea += f.area();
            summaryPrice += f.getPrice();
        }
        if (furnitureSummaryArea <= room.area()) {
            List<Furniture> furnitureList = new ArrayList<>(Arrays.asList(furnitures));
            room.setFurnitures(furnitureList);
            resources.add(room);
            resources.addAll(furnitureList);
            logger.info("Summary price: " + summaryPrice.toString());
        } else {
            throw new ExceededRoomAreaException();
        }
    }

    // counts rooms total area
    public Double countRoomsTotalArea() {
        /*return resources.stream()
                .filter(r -> r instanceof RectangularRoom)
                .mapToDouble(Measurable::area)
                .sum();*/
        double sum = 0d;
        for(Measurable r: resources){
            if(r instanceof RectangularRoom){
                sum+= r.area();
            }
        }
        return sum;
    }

    // logs all recources
    public void showAllRecources() {
        logger.info("Available resources:");
        resources.forEach(r -> logger.info(r.show()));
    }
}
