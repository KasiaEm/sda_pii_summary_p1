package com.sda.ex16;

import java.util.List;

public class RectangularRoom implements Measurable {
    private Double width;
    private Double length;
    private List<Furniture> furnitures;

    public RectangularRoom(Double width, Double length) {
        this.width = width;
        this.length = length;
    }

    @Override
    public Double area() {
        return width * length;
    }

    @Override
    public String show() {
        return "Room " + width + "*" + length;
    }

    public Double getWidth() {
        return width;
    }

    public List<Furniture> getFurnitures() {
        return furnitures;
    }

    public void setFurnitures(List<Furniture> furnitures) {
        this.furnitures = furnitures;
    }
}
