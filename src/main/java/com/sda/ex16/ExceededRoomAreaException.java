package com.sda.ex16;

public class ExceededRoomAreaException extends RuntimeException{
    public ExceededRoomAreaException(){
        super("Exceeded room area.");
    }
}
