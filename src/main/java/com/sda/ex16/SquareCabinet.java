package com.sda.ex16;

public class SquareCabinet extends Furniture {
    private Double length;

    public SquareCabinet(Double length, Double price) {
        super(price);
        this.length = length;
    }

    @Override
    public Double area() {
        return length * length;
    }

    @Override
    public String show() {
        return "Square cabinet " + length + " width.";
    }

    public double getLength() {
        return length;
    }
}
