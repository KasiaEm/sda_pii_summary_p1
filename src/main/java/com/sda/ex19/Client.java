package com.sda.ex19;

import java.time.LocalDate;
import java.util.List;

public class Client {
    private String fullName;
    private LocalDate birthday;
    private List<Book> borrowedBooks;

    public Client(String fullName, LocalDate birthday) {
        this.fullName = fullName;
        this.birthday = birthday;
    }

    public String getFullName() {
        return fullName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public List<Book> getBorrowedBooks() {
        return borrowedBooks;
    }

    public void setBorrowedBooks(List<Book> borrowedBooks) {
        this.borrowedBooks = borrowedBooks;
    }
}
