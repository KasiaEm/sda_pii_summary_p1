package com.sda.ex19;

import java.time.Clock;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

public class BookManager {
    private Map<String, Book> books;
    private Map<String, Client> clients;
    private Logger logger = Logger.getLogger(BookManager.class.getName());
    private Clock clock;

    public BookManager(List<Book> books, List<Client> clients, Clock clock) {
        //stream
        this.books = books
                .stream()
                .collect(Collectors.toMap(Book::getTitle, b -> b));
        this.clients = clients
                .stream()
                .collect(Collectors.toMap(Client::getFullName, c -> c));
        //loop
        /*for (Book book : books) {
            this.books.put(book.getTitle(), book);
        }
        for (Client client : clients) {
            this.clients.put(client.getFullName(), client);
        }*/

        this.clock = clock;
    }

    public void borrowBooks(String clientName, String... toBorrow) {
        Client client = findClient(clientName);
        List<Book> booksAvailable = findBooksAvailable(toBorrow);
        booksAvailable.forEach(b -> b.setBorrowDate(LocalDate.now(clock)));
        client.getBorrowedBooks().addAll(booksAvailable);
        logger.info(booksAvailable.size() + " books borrowed for client " + clientName);
    }

    public void returnBooks(String clientName) {
        Client client = findClient(clientName);
        List<Book> booksToReturn = client.getBorrowedBooks();
        Double penalty = 0d;
        if (!booksToReturn.isEmpty()) {
            penalty = checkForPenalty(client);
            booksToReturn.forEach(b -> b.setBorrowDate(null));
            client.setBorrowedBooks(null);
            logger.info("Books returned from client " + clientName + ". Penalty: " + penalty.toString().substring(0, 3));
        } else {
            logger.info("No books to return.");
        }
    }

    private Client findClient(String clientName) {
        Client client = clients.get(clientName);
        if (client == null) {
            throw new BookManagerException("Client not found.");
        }
        return client;
    }

    private List<Book> findBooksAvailable(String... booksToFind) {
        List<Book> result = new LinkedList<>();
        for (String bookName : booksToFind) {
            Book book = this.books.get(bookName);
            if (book != null && book.getBorrowDate() == null) {
                result.add(book);
            }
        }
        if (result.isEmpty()) {
            throw new BookManagerException("Books not found.");
        }
        return result;
    }

    public Double checkForPenalty(Client client) {
        List<Book> borrowedBooks = client.getBorrowedBooks();
        Double penalty = 0d;
        Double smallestPenalty = 0d;
        for (Book book : borrowedBooks) {
            Double tempPenalty = 0d;
            long days = DAYS.between(book.getBorrowDate(), LocalDate.now(clock)) - 16;
            int counter = 0;
            while (days > 0) {
                if (counter < 28) {
                    tempPenalty += 0.5;
                    days -= 7;
                    counter += 7;
                } else if (counter < 56) {
                    tempPenalty += 0.7;
                    days -= 7;
                    counter += 7;
                } else {
                    tempPenalty += 0.2;
                    days--;
                    counter++;
                }
            }
            if (tempPenalty > 0d) {
                if (DAYS.between(book.getPublishedDate(), book.getBorrowDate()) < 30) {
                    tempPenalty *= 1.1;
                }
                if (smallestPenalty == 0d || tempPenalty < smallestPenalty) {
                    smallestPenalty = tempPenalty;
                }
            }
            penalty += tempPenalty;
        }
        if (client.getBirthday().equals(LocalDate.now())) {
            penalty -= smallestPenalty;
        }
        return penalty;
    }
}

