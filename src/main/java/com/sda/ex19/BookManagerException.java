package com.sda.ex19;

public class BookManagerException extends RuntimeException {
    public BookManagerException(String message){
        super(message);
    }
}
